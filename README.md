Synapse-agent command line client and python library.
See the [Synapse-Agent repository](https://github.com/comodit/synapse-agent) for
more details.
